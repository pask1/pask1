### Hi there, I'm Nils 👋

- 🔭 I’m currently working on: Setting up a homelab Docker/k8s stack
- 🌱 I’m currently learning: C/C++, ZFS, Docker, Kubernetes
- 👯 I’m looking to collaborate on: A multi-platform Rubik's Cube timer app, or an advanced chess clock app
- 🤔 I’m looking for help with: How to dynamically display LaTeX equations in a React component
- 💬 Ask me about: Rubik's cube methods
- 📫 How to reach me: mailto: nils.korsfeldt@gmail.com
